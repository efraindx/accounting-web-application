USE [Contabilidad]
GO

/****** Object:  Table [dbo].[CuentaContable]    Script Date: 11/6/2016 9:53:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CuentaContable](
	[CuentaContableID] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](50) NULL,
	[TipoCuentaID] [int] NULL,
	[PermiteTransacciones] [varchar](5) NULL,
	[Nivel] [int] NULL,
	[CuentaMayorID] [int] NULL,
	[Balance] [float] NULL,
	[EstadoID] [int] NULL,
 CONSTRAINT [PK_CuentaContable_1] PRIMARY KEY CLUSTERED 
(
	[CuentaContableID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[CuentaContable]  WITH CHECK ADD  CONSTRAINT [FK_CuentaContable_CuentaMayor] FOREIGN KEY([CuentaMayorID])
REFERENCES [dbo].[CuentaMayor] ([CuentaMayorID])
GO

ALTER TABLE [dbo].[CuentaContable] CHECK CONSTRAINT [FK_CuentaContable_CuentaMayor]
GO

ALTER TABLE [dbo].[CuentaContable]  WITH CHECK ADD  CONSTRAINT [FK_CuentaContable_Estado] FOREIGN KEY([EstadoID])
REFERENCES [dbo].[Estado] ([EstadoID])
GO

ALTER TABLE [dbo].[CuentaContable] CHECK CONSTRAINT [FK_CuentaContable_Estado]
GO

ALTER TABLE [dbo].[CuentaContable]  WITH CHECK ADD  CONSTRAINT [FK_CuentaContable_TipoCuenta] FOREIGN KEY([TipoCuentaID])
REFERENCES [dbo].[TipoCuenta] ([TipoCuentaID])
GO

ALTER TABLE [dbo].[CuentaContable] CHECK CONSTRAINT [FK_CuentaContable_TipoCuenta]
GO


