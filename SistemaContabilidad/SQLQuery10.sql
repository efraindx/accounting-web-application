USE [Contabilidad]
GO

/****** Object:  View [dbo].[EntradaContableView]    Script Date: 11/6/2016 9:54:28 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[EntradaContableView]
AS
SELECT        dbo.EntradaContable.EntradaContableID, dbo.EntradaContable.Descripcion, dbo.Auxiliar.Descripcion AS Expr1, dbo.Origen.Descripcion AS Expr2, dbo.EntradaContable.Fecha, dbo.EntradaContable.Monto, 
                         dbo.Estado.Descripcion AS Expr3, dbo.CuentaContable.Descripcion AS Expr4
FROM            dbo.EntradaContable CROSS JOIN
                         dbo.Auxiliar CROSS JOIN
                         dbo.Origen CROSS JOIN
                         dbo.Estado CROSS JOIN
                         dbo.CuentaContable
WHERE        (dbo.Auxiliar.AuxiliarID LIKE dbo.EntradaContable.AuxiliarID) AND (dbo.Origen.OrigenID LIKE dbo.EntradaContable.OrigenID) AND (dbo.Estado.EstadoID LIKE dbo.EntradaContable.EstadoID) AND 
                         (dbo.CuentaContable.CuentaContableID LIKE dbo.EntradaContable.CuentaContableID)

GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "EntradaContable"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 209
               Right = 226
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Auxiliar"
            Begin Extent = 
               Top = 6
               Left = 264
               Bottom = 102
               Right = 434
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Origen"
            Begin Extent = 
               Top = 6
               Left = 472
               Bottom = 102
               Right = 642
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "CuentaContable"
            Begin Extent = 
               Top = 102
               Left = 264
               Bottom = 232
               Right = 469
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Estado"
            Begin Extent = 
               Top = 6
               Left = 680
               Bottom = 102
               Right = 850
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
      ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'EntradaContableView'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'   Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'EntradaContableView'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'EntradaContableView'
GO


