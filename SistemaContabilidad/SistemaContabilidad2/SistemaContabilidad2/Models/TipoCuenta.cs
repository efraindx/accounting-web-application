namespace SistemaContabilidad2.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TipoCuenta")]
    public partial class TipoCuenta
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TipoCuenta()
        {
            CuentaContables = new HashSet<CuentaContable>();
        }

        public int TipoCuentaID { get; set; }

        [StringLength(50)]
        public string Descripcion { get; set; }

        public int? OrigenID { get; set; }

        public int? EstadoID { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CuentaContable> CuentaContables { get; set; }

        public virtual Estado Estado { get; set; }
    }
}
