namespace SistemaContabilidad2.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CuentaContable")]
    public partial class CuentaContable
    {
        public int CuentaContableID { get; set; }

        [StringLength(50)]
        public string Descripcion { get; set; }

        public int? TipoCuentaID { get; set; }

        [StringLength(5)]
        public string PermiteTransacciones { get; set; }

        public int? Nivel { get; set; }

        public int? CuentaMayorID { get; set; }

        public double? Balance { get; set; }

        public int? EstadoID { get; set; }

        public virtual Estado Estado { get; set; }

        public virtual TipoCuenta TipoCuenta { get; set; }
    }
}
