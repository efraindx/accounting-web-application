namespace SistemaContabilidad2.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("EntradaContable")]
    public partial class EntradaContable
    {
        public int EntradaContableID { get; set; }

        [StringLength(50)]
        public string Descripcion { get; set; }

        public int? AuxiliarID { get; set; }

        public int? OrigenID { get; set; }

        [Column(TypeName = "date")]
        public DateTime? Fecha { get; set; }

        public double? Monto { get; set; }

        public int? EstadoID { get; set; }

        public int? CuentaContableID { get; set; }
    }
}
