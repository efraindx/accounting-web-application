namespace SistemaContabilidad2.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TipoMoneda")]
    public partial class TipoMoneda
    {
        public int TipoMonedaID { get; set; }

        [StringLength(50)]
        public string Descripcion { get; set; }

        public double? Tasa { get; set; }

        public int? EstadoID { get; set; }

        public virtual Estado Estado { get; set; }
    }
}
