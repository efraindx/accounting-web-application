﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SistemaContabilidad2.Models;

namespace SistemaContabilidad2
{
    public class EntradaContablesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: EntradaContables
        [Authorize]
        public ActionResult Index()
        {
            return View(db.EntradaContables.ToList());
        }

        // GET: EntradaContables/Details/5
        [Authorize]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EntradaContable entradaContable = db.EntradaContables.Find(id);
            if (entradaContable == null)
            {
                return HttpNotFound();
            }
            return View(entradaContable);
        }

        // GET: EntradaContables/Create
        [Authorize]
        public ActionResult Create()
        {
            return View();
        }

        // POST: EntradaContables/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "EntradaContableID,Descripcion,AuxiliarID,OrigenID,Fecha,Monto,EstadoID,CuentaContableID")] EntradaContable entradaContable)
        {
            if (ModelState.IsValid)
            {
                db.EntradaContables.Add(entradaContable);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(entradaContable);
        }

        // GET: EntradaContables/Edit/5
        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EntradaContable entradaContable = db.EntradaContables.Find(id);
            if (entradaContable == null)
            {
                return HttpNotFound();
            }
            return View(entradaContable);
        }

        // POST: EntradaContables/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "EntradaContableID,Descripcion,AuxiliarID,OrigenID,Fecha,Monto,EstadoID,CuentaContableID")] EntradaContable entradaContable)
        {
            if (ModelState.IsValid)
            {
                db.Entry(entradaContable).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(entradaContable);
        }

        // GET: EntradaContables/Delete/5
        [Authorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EntradaContable entradaContable = db.EntradaContables.Find(id);
            if (entradaContable == null)
            {
                return HttpNotFound();
            }
            return View(entradaContable);
        }

        // POST: EntradaContables/Delete/5
        [Authorize]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            EntradaContable entradaContable = db.EntradaContables.Find(id);
            db.EntradaContables.Remove(entradaContable);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
