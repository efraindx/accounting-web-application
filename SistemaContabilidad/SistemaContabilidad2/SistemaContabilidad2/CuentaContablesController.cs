﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SistemaContabilidad2.Models;

namespace SistemaContabilidad2
{
    public class CuentaContablesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();


        // GET: CuentaContables
        [Authorize]
        public ActionResult Index()
        {
            return View(db.CuentaContables.ToList());
        }


        // GET: CuentaContables/Details/5
        [Authorize]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CuentaContable cuentaContable = db.CuentaContables.Find(id);
            if (cuentaContable == null)
            {
                return HttpNotFound();
            }
            return View(cuentaContable);
        }

        // GET: CuentaContables/Create
        [Authorize]
        public ActionResult Create()
        {
            return View();
        }

        // POST: CuentaContables/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CuentaContableID,Descripcion,TipoCuentaID,PermiteTransacciones,Nivel,CuentaMayorID,Balance,EstadoID")] CuentaContable cuentaContable)
        {
            if (ModelState.IsValid)
            {
                db.CuentaContables.Add(cuentaContable);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(cuentaContable);
        }

        // GET: CuentaContables/Edit/5
        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CuentaContable cuentaContable = db.CuentaContables.Find(id);
            if (cuentaContable == null)
            {
                return HttpNotFound();
            }
            return View(cuentaContable);
        }

        // POST: CuentaContables/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CuentaContableID,Descripcion,TipoCuentaID,PermiteTransacciones,Nivel,CuentaMayorID,Balance,EstadoID")] CuentaContable cuentaContable)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cuentaContable).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(cuentaContable);
        }

        // GET: CuentaContables/Delete/5
        [Authorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CuentaContable cuentaContable = db.CuentaContables.Find(id);
            if (cuentaContable == null)
            {
                return HttpNotFound();
            }
            return View(cuentaContable);
        }

        // POST: CuentaContables/Delete/5
        [Authorize]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CuentaContable cuentaContable = db.CuentaContables.Find(id);
            db.CuentaContables.Remove(cuentaContable);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
