﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SistemaContabilidad2.Models;

namespace SistemaContabilidad2
{
    public class TipoMonedasController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: TipoMonedas
        [Authorize]
        public ActionResult Index()
        {
            var tipoMonedas = db.TipoMonedas.Include(t => t.Estado);
            return View(tipoMonedas.ToList());
        }

        // GET: TipoMonedas/Details/5
        [Authorize]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoMoneda tipoMoneda = db.TipoMonedas.Find(id);
            if (tipoMoneda == null)
            {
                return HttpNotFound();
            }
            return View(tipoMoneda);
        }

        // GET: TipoMonedas/Create
        [Authorize]
        public ActionResult Create()
        {
            ViewBag.EstadoID = new SelectList(db.Estadoes, "EstadoID", "Descripcion");
            return View();
        }

        // POST: TipoMonedas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TipoMonedaID,Descripcion,Tasa,EstadoID")] TipoMoneda tipoMoneda)
        {
            if (ModelState.IsValid)
            {
                db.TipoMonedas.Add(tipoMoneda);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.EstadoID = new SelectList(db.Estadoes, "EstadoID", "Descripcion", tipoMoneda.EstadoID);
            return View(tipoMoneda);
        }

        // GET: TipoMonedas/Edit/5
        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoMoneda tipoMoneda = db.TipoMonedas.Find(id);
            if (tipoMoneda == null)
            {
                return HttpNotFound();
            }
            ViewBag.EstadoID = new SelectList(db.Estadoes, "EstadoID", "Descripcion", tipoMoneda.EstadoID);
            return View(tipoMoneda);
        }

        // POST: TipoMonedas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "TipoMonedaID,Descripcion,Tasa,EstadoID")] TipoMoneda tipoMoneda)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tipoMoneda).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.EstadoID = new SelectList(db.Estadoes, "EstadoID", "Descripcion", tipoMoneda.EstadoID);
            return View(tipoMoneda);
        }

        // GET: TipoMonedas/Delete/5
        [Authorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoMoneda tipoMoneda = db.TipoMonedas.Find(id);
            if (tipoMoneda == null)
            {
                return HttpNotFound();
            }
            return View(tipoMoneda);
        }

        // POST: TipoMonedas/Delete/5
        [Authorize]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TipoMoneda tipoMoneda = db.TipoMonedas.Find(id);
            db.TipoMonedas.Remove(tipoMoneda);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
