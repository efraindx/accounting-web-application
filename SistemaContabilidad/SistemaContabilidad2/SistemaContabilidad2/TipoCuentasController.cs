﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SistemaContabilidad2.Models;

namespace SistemaContabilidad2
{
    public class TipoCuentasController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: TipoCuentas
        [Authorize]
        public ActionResult Index()
        {
            var tipoCuentas = db.TipoCuentas.Include(t => t.Estado);
            return View(tipoCuentas.ToList());
        }

        // GET: TipoCuentas/Details/5
        [Authorize]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoCuenta tipoCuenta = db.TipoCuentas.Find(id);
            if (tipoCuenta == null)
            {
                return HttpNotFound();
            }
            return View(tipoCuenta);
        }

        // GET: TipoCuentas/Create
        [Authorize]
        public ActionResult Create()
        {
            ViewBag.EstadoID = new SelectList(db.Estadoes, "EstadoID", "Descripcion");
            return View();
        }

        // POST: TipoCuentas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TipoCuentaID,Descripcion,OrigenID,EstadoID")] TipoCuenta tipoCuenta)
        {
            if (ModelState.IsValid)
            {
                db.TipoCuentas.Add(tipoCuenta);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.EstadoID = new SelectList(db.Estadoes, "EstadoID", "Descripcion", tipoCuenta.EstadoID);
            return View(tipoCuenta);
        }

        // GET: TipoCuentas/Edit/5
        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoCuenta tipoCuenta = db.TipoCuentas.Find(id);
            if (tipoCuenta == null)
            {
                return HttpNotFound();
            }
            ViewBag.EstadoID = new SelectList(db.Estadoes, "EstadoID", "Descripcion", tipoCuenta.EstadoID);
            return View(tipoCuenta);
        }

        // POST: TipoCuentas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "TipoCuentaID,Descripcion,OrigenID,EstadoID")] TipoCuenta tipoCuenta)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tipoCuenta).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.EstadoID = new SelectList(db.Estadoes, "EstadoID", "Descripcion", tipoCuenta.EstadoID);
            return View(tipoCuenta);
        }

        // GET: TipoCuentas/Delete/5
        [Authorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoCuenta tipoCuenta = db.TipoCuentas.Find(id);
            if (tipoCuenta == null)
            {
                return HttpNotFound();
            }
            return View(tipoCuenta);
        }

        // POST: TipoCuentas/Delete/5
        [Authorize]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TipoCuenta tipoCuenta = db.TipoCuentas.Find(id);
            db.TipoCuentas.Remove(tipoCuenta);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
