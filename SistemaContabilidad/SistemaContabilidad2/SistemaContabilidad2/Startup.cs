﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SistemaContabilidad2.Startup))]
namespace SistemaContabilidad2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
