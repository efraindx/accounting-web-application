USE [Contabilidad]
GO

/****** Object:  Table [dbo].[TipoCuenta]    Script Date: 11/6/2016 9:43:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TipoCuenta](
	[TipoCuentaID] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](50) NULL,
	[OrigenID] [int] NULL,
	[EstadoID] [int] NULL,
 CONSTRAINT [PK_TipoCuenta] PRIMARY KEY CLUSTERED 
(
	[TipoCuentaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[TipoCuenta]  WITH CHECK ADD  CONSTRAINT [FK_TipoCuenta_Estado] FOREIGN KEY([EstadoID])
REFERENCES [dbo].[Estado] ([EstadoID])
GO

ALTER TABLE [dbo].[TipoCuenta] CHECK CONSTRAINT [FK_TipoCuenta_Estado]
GO

ALTER TABLE [dbo].[TipoCuenta]  WITH CHECK ADD  CONSTRAINT [FK_TipoCuenta_Origen] FOREIGN KEY([OrigenID])
REFERENCES [dbo].[Origen] ([OrigenID])
GO

ALTER TABLE [dbo].[TipoCuenta] CHECK CONSTRAINT [FK_TipoCuenta_Origen]
GO


