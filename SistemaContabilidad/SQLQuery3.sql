USE [Contabilidad]
GO

/****** Object:  Table [dbo].[TipoMoneda]    Script Date: 11/6/2016 9:42:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TipoMoneda](
	[TipoMonedaID] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](50) NULL,
	[Tasa] [float] NULL,
	[EstadoID] [int] NULL,
 CONSTRAINT [PK_TipoMoneda] PRIMARY KEY CLUSTERED 
(
	[TipoMonedaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[TipoMoneda]  WITH CHECK ADD  CONSTRAINT [FK_TipoMoneda_Estado] FOREIGN KEY([EstadoID])
REFERENCES [dbo].[Estado] ([EstadoID])
GO

ALTER TABLE [dbo].[TipoMoneda] CHECK CONSTRAINT [FK_TipoMoneda_Estado]
GO


