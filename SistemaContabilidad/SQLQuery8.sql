USE [Contabilidad]
GO

/****** Object:  Table [dbo].[EntradaContable]    Script Date: 11/6/2016 9:53:53 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[EntradaContable](
	[EntradaContableID] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](50) NULL,
	[AuxiliarID] [int] NULL,
	[OrigenID] [int] NULL,
	[Fecha] [date] NULL,
	[Monto] [float] NULL,
	[EstadoID] [int] NULL,
	[CuentaContableID] [int] NULL,
 CONSTRAINT [PK_CuentaContable] PRIMARY KEY CLUSTERED 
(
	[EntradaContableID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[EntradaContable]  WITH CHECK ADD  CONSTRAINT [FK_EntradaContable_Auxiliar] FOREIGN KEY([AuxiliarID])
REFERENCES [dbo].[Auxiliar] ([AuxiliarID])
GO

ALTER TABLE [dbo].[EntradaContable] CHECK CONSTRAINT [FK_EntradaContable_Auxiliar]
GO

ALTER TABLE [dbo].[EntradaContable]  WITH CHECK ADD  CONSTRAINT [FK_EntradaContable_CuentaContable] FOREIGN KEY([CuentaContableID])
REFERENCES [dbo].[CuentaContable] ([CuentaContableID])
GO

ALTER TABLE [dbo].[EntradaContable] CHECK CONSTRAINT [FK_EntradaContable_CuentaContable]
GO

ALTER TABLE [dbo].[EntradaContable]  WITH CHECK ADD  CONSTRAINT [FK_EntradaContable_Estado] FOREIGN KEY([EstadoID])
REFERENCES [dbo].[Estado] ([EstadoID])
GO

ALTER TABLE [dbo].[EntradaContable] CHECK CONSTRAINT [FK_EntradaContable_Estado]
GO

ALTER TABLE [dbo].[EntradaContable]  WITH CHECK ADD  CONSTRAINT [FK_EntradaContable_Origen] FOREIGN KEY([OrigenID])
REFERENCES [dbo].[Origen] ([OrigenID])
GO

ALTER TABLE [dbo].[EntradaContable] CHECK CONSTRAINT [FK_EntradaContable_Origen]
GO


